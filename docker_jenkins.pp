# Installer le docker
package{
  'docker.io':
  ensure=>installed,
  notify=>Service['docker']
}

# Verifier qu'il tourne
service{
  'docker':
    ensure=>running
}

# Créer un dossier jenkins_home
file {
  '/home/vagrant/jenkins_home':
    ensure=>directory
}

# Créer un container de jenkins
exec {
  '/usr/bin/docker run -d -p 8080:8080 -v /home/vagrant/jenkins_home:/var/jenkins_home jenkins:2.60.3':
   onlyif   =>['/usr/bin/test -d /home/vagrant/jenkins_home','/usr/bin/test -x /usr/bin/docker']
}
